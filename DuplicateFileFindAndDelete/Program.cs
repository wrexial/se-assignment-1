﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DuplicateFileFindAndDelete
{
    // ToDo:
    // change license supplied: updateable by author, and user has to stay current to updates
    // court of law of author's choice

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
