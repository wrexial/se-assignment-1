﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileFindAndDeleteLibrary
{
    public enum FindAndDeleteOptions
    {
        KeepOlderFileOnly,
        KeepNewerFileOnly,
        CreateDuplicateFileReportOnly
    }
}
