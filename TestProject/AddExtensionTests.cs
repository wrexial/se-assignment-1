﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Etier.IconHelper;
using System.Collections;
using System.Windows.Forms;
namespace TestProject
{
    [TestClass]
    public class AddExtensionTests
    {
        private ImageList imageList = new ImageList();
        private IconReader.IconSize iconSize = 0;

        [TestMethod]
        public void SingleExtension()
        {
            //used in both
            string extension = ".jpg";
            int imageListPosition = 0;
            //expected
            Hashtable extensionExpected = new Hashtable();
            extensionExpected.Add(extension, imageListPosition);
            //Actual
            IconListManager target = new IconListManager(imageList, iconSize);
            target.AddExtension(extension, imageListPosition);
            //Assert
            CollectionAssert.AreEqual(extensionExpected, target._extensionList);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InvalidExtension()
        {
            //used in both
            string extension = null;
            int imageListPosition = 0;
            //expected
            Hashtable extensionExpected = new Hashtable();
            extensionExpected.Add(extension, imageListPosition);
            //Actual
            IconListManager target = new IconListManager(imageList, iconSize);
            target.AddExtension(extension, imageListPosition);
            //Assert
            CollectionAssert.AreEqual(extensionExpected, target._extensionList);
        }

        [TestMethod]
        public void EmptyExtension()
        {
            //used in both
            string extension = "";
            int imageListPosition = 0;
            //expected
            Hashtable extensionExpected = new Hashtable();
            extensionExpected.Add(extension, imageListPosition);
            //Actual
            IconListManager target = new IconListManager(imageList, iconSize);
            target.AddExtension(extension, imageListPosition);
            //Assert
            CollectionAssert.AreEqual(extensionExpected, target._extensionList);
        }


        [TestMethod]
        public void MultipleExtension()
        {
            //used in both
            string[] extensions = {".jpg",".txt",".boo"};
            //expected
            Hashtable extensionExpected = new Hashtable();

            for(int e = 0; e < extensions.Length; e++)
                extensionExpected.Add(extensions[e], e);
            //Actual
            IconListManager target = new IconListManager(imageList, iconSize);
            for (int e = 0; e < extensions.Length; e++)
                target.AddExtension(extensions[e], e); 
            //Assert
            CollectionAssert.AreEqual(extensionExpected, target._extensionList);

        }

        [TestMethod]
        public void IllogicalExtension()
        {
            //used in both
            string[] extensions = { ".jpg", "thisDoesntExist", String.Empty };
            //expected
            Hashtable extensionExpected = new Hashtable();

            for (int e = 0; e < extensions.Length; e++)
                extensionExpected.Add(extensions[e], e);
            //Actual
            IconListManager target = new IconListManager(imageList, iconSize);
            for (int e = 0; e < extensions.Length; e++)
                target.AddExtension(extensions[e], e);
            //Assert
            CollectionAssert.AreEqual(extensionExpected, target._extensionList);
        }

        [TestMethod]
        public void invalidPosition()
        {
            //used in both
            string extension = ".jpg";
            int imageListPosition = -3;
            //expected
            Hashtable extensionExpected = new Hashtable();
            extensionExpected.Add(extension, imageListPosition);
            //Actual
            IconListManager target = new IconListManager(imageList, iconSize);
            target.AddExtension(extension, imageListPosition);
            //Assert
            CollectionAssert.AreEqual(extensionExpected, target._extensionList);
        }

    }
}
