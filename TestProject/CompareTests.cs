﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileFindAndDeleteLibrary;
using DuplicateFileFindAndDelete;
namespace TestProject
{
    [TestClass]
    public class CompareTests
    {
        [TestMethod()]
        public void Same()
        {
            DateTimeComparer target = new DateTimeComparer();
            DateTime x = DateTime.Now;
            DateTime y = x;
            int expected = -1;
            int actual;
            actual = target.Compare(x, y);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        [TestMethod()]
        public void Less()
        {
            DateTimeComparer target = new DateTimeComparer();
            DateTime x = DateTime.Now.AddDays(-1);
            DateTime y = DateTime.Now;
            int expected = -1;
            int actual;
            actual = target.Compare(x, y);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        [TestMethod()]
        public void Greater()
        {
            DateTimeComparer target = new DateTimeComparer();
            DateTime x = DateTime.Now.AddDays(1);
            DateTime y = DateTime.Now;
            int expected = 1;
            int actual;
            actual = target.Compare(x, y);
            Assert.AreEqual(expected, actual);
            // Assert.Inconclusive("Verify the correctness of this test method.");
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Invalid()
        {
            DateTimeComparer target = new DateTimeComparer();
            DateTime x = DateTime.MaxValue.AddDays(1);
            DateTime y = DateTime.Now;
            int expected = 0;
            int actual;
            actual = target.Compare(x, y);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
