﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileFindAndDeleteLibrary;
using DuplicateFileFindAndDelete;
using Etier.IconHelper;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.IO;

namespace TestProject
{
    [TestClass]
    public class AddFileTests
    {
        [TestMethod]
        public void Valid()
        {
            
            string url = "C:\\Users\\Wrexial\\Desktop\\TestFolder\\New folder";
            FileInfo fi = new FileInfo(url);

            Dictionary<string, FileInfo> expected = new Dictionary<string, FileInfo>();

            expected.Add(fi.DirectoryName + fi.Name, fi);

            UniqueFileRepresentation UFR = new FileFindAndDeleteLibrary.UniqueFileRepresentation();
            UFR.AddFile(fi);

            CollectionAssert.AreEqual(expected, UFR.filesRepesented);
        }

        [TestMethod]
        public void Unavailable()
        {

            string url = "C:\\This\\Doesnt\\Exists";
            FileInfo fi = new FileInfo(url);

            Dictionary<string, FileInfo> expected = new Dictionary<string, FileInfo>();

            expected.Add(fi.DirectoryName + fi.Name, fi);

            UniqueFileRepresentation UFR = new FileFindAndDeleteLibrary.UniqueFileRepresentation();
            UFR.AddFile(fi);

            CollectionAssert.AreEqual(expected, UFR.filesRepesented);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Invalid()
        {

            string url = "";
            FileInfo fi = new FileInfo(url);

            Dictionary<string, FileInfo> expected = new Dictionary<string, FileInfo>();

            expected.Add(fi.DirectoryName + fi.Name, fi);

            UniqueFileRepresentation UFR = new FileFindAndDeleteLibrary.UniqueFileRepresentation();
            UFR.AddFile(fi);

            CollectionAssert.AreEqual(expected, UFR.filesRepesented);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Null()
        {

            string url = null;
            FileInfo fi = new FileInfo(url);

            Dictionary<string, FileInfo> expected = new Dictionary<string, FileInfo>();

            expected.Add(fi.DirectoryName + fi.Name, fi);

            UniqueFileRepresentation UFR = new FileFindAndDeleteLibrary.UniqueFileRepresentation();
            UFR.AddFile(fi);

            CollectionAssert.AreEqual(expected, UFR.filesRepesented);
        }
    }
}