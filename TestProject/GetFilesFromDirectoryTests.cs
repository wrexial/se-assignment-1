﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileFindAndDeleteLibrary;
using DuplicateFileFindAndDelete;
using System.Collections.Generic;
using System.IO;

namespace TestProject
{
    [TestClass]
    public class GetFilesFromDirectoryTests
    {
        [TestMethod]
        public void SingleFilter()
        {
            string folder = "C:\\Users\\Wrexial\\Desktop\\TestFolder";
            string[] filters = new String[1];
            filters[0] = "*.txt";
            int expected = 1;
            int actual = 0;
            IEnumerable<FileInfo> returnData = FindAndDelete.GetFilesFromDirectory(folder, filters);

            foreach (FileInfo info in returnData)
            {
                actual++;
            }
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MultipleFilter()
        {
            string folder = "C:\\Users\\Wrexial\\Desktop\\TestFolder";
            string[] filters = new String[2];
            filters[0] = "*.txt";
            filters[1] = "*.doc";
            int expected = 2;
            int actual = 0;
            IEnumerable<FileInfo> returnData = FindAndDelete.GetFilesFromDirectory(folder, filters);

            foreach (FileInfo info in returnData)
            {
                actual++;
            }
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void EmptyFilter()
        {
            string folder = "C:\\Users\\Wrexial\\Desktop\\TestFolder";
            string[] filters = new String[0];
            int expected = 0;
            int actual = 0;
            IEnumerable<FileInfo> returnData = FindAndDelete.GetFilesFromDirectory(folder, filters);

            foreach (FileInfo info in returnData)
            {
                actual++;
            }
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void NullFilter()
        {
            string folder = "C:\\Users\\Wrexial\\Desktop\\TestFolder";
            string[] filters = null;
            IEnumerable<FileInfo> returnData = FindAndDelete.GetFilesFromDirectory(folder, filters);
        }

        [TestMethod]
        [ExpectedException(typeof(DirectoryNotFoundException))]
        public void InvalidFolder()
        {
            string folder = "C:\\This\\Doesnt\\Exists";
            string[] filters = new String[1];
            filters[0] = "*.txt";
            IEnumerable<FileInfo> returnData = FindAndDelete.GetFilesFromDirectory(folder, filters);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EmptyStringFolder()
        {
            string folder = "";
            string[] filters = new String[1];
            filters[0] = "*.txt";
            IEnumerable<FileInfo> returnData = FindAndDelete.GetFilesFromDirectory(folder, filters);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullFolder()
        {
            string folder = null;
            string[] filters = new String[1];
            filters[0] = "*.txt";
            IEnumerable<FileInfo> returnData = FindAndDelete.GetFilesFromDirectory(folder, filters);
        }
    }
}
